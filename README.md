# Object-Oriented Programming Languages III final project #
* Przemysław Petka - Bitcoin Exchange stock simulator
* Project simulates bitcoin changing course on chart. User has ability to invest money and (depending on his skill and prediction) gain more or lose all that he invested
* To run programe import project to intelij  and run StockExchangeDisplay class
* At the begining your have to put your name. You are starting with 100 $. Now whole investing process is up to you (you can do it via another smaller panel)

## Requirements for project
In my project i didnt include locks or synchronisation although i had a problem that i solved. Problem was that i was adding points to ArrayList and in the same time another thread was iterating over whole collection. I could lock addint to list untill loop is over but i fould CopyOnWriteArrayList container that did it for me. There is no reason to reinvent the wheel, is there?
In my project i didnt include 2nd Interface and Polimorphism (although i know how to use it, you can check my knowledge about polimorphis on oral exam if you like) becouse i did not see any need to use it. It would bring unnesesary complication which is not good at all. I am writeing this note becouse i would like not to lose points. I did alot of efford to do this. I Hope you enjoy.

Yours sincerely

