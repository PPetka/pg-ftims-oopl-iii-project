package currency;


public interface Currency {
     Integer getCurrentExchangeRate();
     Integer getHighestExchangeRate();
     Integer getLowestExchangeRate();
}
