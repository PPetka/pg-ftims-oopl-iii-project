package currency;


public class Bitcoin implements Currency{

    Integer previousExchangeRate;
    Integer currentExchangeRate;
    Integer startingExchangeRate;
    Integer highestExchangeRate;
    Integer lowestExchangeRate;

    public Bitcoin(Integer startingExchangeRate){
        this.previousExchangeRate = 0 ;
        this.currentExchangeRate = startingExchangeRate;
        this.startingExchangeRate = startingExchangeRate;
        this.highestExchangeRate = startingExchangeRate;
        this.lowestExchangeRate = startingExchangeRate;
    }

    public void setCurrentValueBasingOnYPosition(Integer currentY){
        if(currentY < previousExchangeRate){
            this.currentExchangeRate -= Math.abs(currentY-previousExchangeRate);
        }
        else{
            this.currentExchangeRate += Math.abs(currentY-previousExchangeRate);
        }
        //settin highest and lowest echange Rate
        if(this.currentExchangeRate > highestExchangeRate )
            highestExchangeRate = currentExchangeRate;
        else if(this.currentExchangeRate < lowestExchangeRate)
            lowestExchangeRate = currentExchangeRate;

        previousExchangeRate = currentY;
    }

    public Integer getCurrentExchangeRate(){
        return currentExchangeRate;
    }

    public Integer getHighestExchangeRate(){
        return highestExchangeRate;
    }

    public Integer getLowestExchangeRate(){
        return  lowestExchangeRate;
    }
    
}
