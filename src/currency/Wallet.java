package currency;

public class Wallet {
    private Double dollars;
    private Double bitcoins;

    public Wallet(){
        dollars = 100.0;
        bitcoins = 0.0;
    }

    public void addDollars(Double money_to_add){
        dollars += money_to_add;
    }

    public void addBitcoins(Double money_to_add){
        bitcoins += money_to_add;
    }

    public Double withdrawBitcoins(Double money_to_withdraw){
        Double temp_money ;
        if(money_to_withdraw <= bitcoins && money_to_withdraw >0){
            temp_money = money_to_withdraw;
            bitcoins-= money_to_withdraw;
        }
        else{
            temp_money = bitcoins;
            bitcoins = 0.0;
        }
        return  temp_money;
    }

    public Double withdrawDollars(Double money_to_withdraw){
        Double temp_money ;
        if(money_to_withdraw <= dollars && money_to_withdraw >0){
            temp_money = money_to_withdraw;
            dollars-= money_to_withdraw;
        }
        else{
            temp_money = dollars;
            dollars = 0.0;
        }
        return  temp_money;
    }

    public Double getDollars(){
        return  dollars;
    }

    public Double getBitcoins(){
        return  bitcoins;
    }
}
