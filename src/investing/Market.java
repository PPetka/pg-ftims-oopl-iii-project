package investing;


import currency.Bitcoin;

import java.text.SimpleDateFormat;
import java.util.*;

public class Market {
    private Investor investor ;
    private Bitcoin bitcoin;
    private Map<String,Double[]> sellHistory;
    private Map<String,Double[]> buyHistory;

    public Market(Bitcoin bitcoin , Investor investor){
        this.bitcoin = bitcoin;
        this.investor= investor;
        this.sellHistory = new TreeMap<>();
        this.buyHistory = new TreeMap<>();
    }

    public void buyBTC(Double ammount){
        if(ammount <= investor.getDollars() && ammount > 0) {
            Double btcAmmount = ammount / bitcoin.getCurrentExchangeRate();
            investor.withdrawDollars(ammount);
            investor.addBitcoins(btcAmmount);
            addToBuyHistory(btcAmmount, ammount);
        }
    }

    public void sellBTC(Double ammount){
        if(ammount > 0 && ammount <= investor.getBitcoins()) {
            Double dollarsAmmount = ammount * bitcoin.getCurrentExchangeRate();
            investor.withdrawBitcoins(ammount);
            investor.addDollars(dollarsAmmount);
            addToSellHistory(ammount,dollarsAmmount);
        }
    }

    public void addToSellHistory(Double btc, Double dollars){
        String date = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").format(Calendar.getInstance().getTime());

        Double[] temp = new Double[3];
        temp[0] = btc;
        temp[1] = dollars;
        sellHistory.put(date, temp);
    }

    public void addToBuyHistory(Double btc, Double dollars) {
        String date = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").format(Calendar.getInstance().getTime());

        Double[] temp = new Double[3];
        temp[0] = btc;
        temp[1] = dollars;
        buyHistory.put(date, temp);
    }

}
