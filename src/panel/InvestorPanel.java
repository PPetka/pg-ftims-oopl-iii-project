package panel;

import investing.Investor;
import investing.Market;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class InvestorPanel  extends JFrame{
    private Investor investor ;
    private Market market ;

    public InvestorPanel(Investor investor, Market market , JFrame motherFrame){
        initComponents();
        setTitle("Exchange");
        setLocationRelativeTo(motherFrame);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(new Dimension(300,150));
        setLayout(null);
        setVisible(true);

        this.investor = investor;
        this.market = market;

    }
    public void initComponents(){
        //label buy
        JLabel label_buy = new JLabel("Buy for $");
        label_buy.setBounds(10,10,75,20);
        this.getContentPane().add(label_buy);
        //Buy Text field
        JTextField buyArea = new JTextField();
        buyArea.setBounds(80,10,100,20);
        this.getContentPane().add(buyArea);
        //CheckBox
        JCheckBox checkBox = new JCheckBox("set max $" ,false);
        checkBox.setBounds(5,35,100,20);
        this.getContentPane().add(checkBox);
        checkBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
              if(!checkBox.isSelected()){
                  buyArea.setText("0");
              }
              else{
                  buyArea.setText( investor.getDollars().toString());
              }
            }
        });


        JButton button_Buy_BTC = new JButton("Buy");
        button_Buy_BTC.setBounds(180,10,120,20);

        this.getContentPane().add(button_Buy_BTC);
        button_Buy_BTC.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                if(Double.parseDouble(buyArea.getText())>0) {
                    market.buyBTC(Double.parseDouble(buyArea.getText()));
                }
            }
        });
//////////////////////////////////

        //label sell
        JLabel label_sell = new JLabel("Sell BTC");
        label_sell.setBounds(10,60,75,20);
        this.getContentPane().add(label_sell);
        //Sell Text field
        JTextField sellArea = new JTextField();
        sellArea.setBounds(80,60,100,20);
        this.getContentPane().add(sellArea);

        //CheckBox
        JCheckBox checkBox_sell = new JCheckBox("set max BTC" ,false);
        checkBox_sell.setBounds(5,85,150,20);
        this.getContentPane().add(checkBox_sell);
        checkBox_sell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                if(!checkBox.isSelected()){
                    sellArea.setText("0");
                }
                else{
                    sellArea.setText( investor.getBitcoins().toString()) ;
                    sellArea.requestFocus();

                }
            }
        });
        //Button
        JButton button_Sell_BTC = new JButton("Sell");
        button_Sell_BTC.setBounds(180,60,120,20);
        this.getContentPane().add(button_Sell_BTC);

        button_Sell_BTC.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                market.sellBTC(Double.parseDouble(sellArea.getText()));
            }
        });
    }

}
