package panel;

import investing.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MainPanel {


    public static void main(String args[]){
        JFrame frame = new JFrame("Stock Exchange Simulator");
        try {
            frame.setContentPane(new JLabel(new ImageIcon("background.jpg")));
        }
        catch(Exception e){
            System.out.println("Background");
        }

        JButton jButton1 = new JButton("Start");
        JLabel label_nickname = new JLabel("Enter your nickname");
        JTextField textField_nickname = new JTextField(10);


        jButton1.setBounds(400,200,100,40);
        label_nickname.setBounds(340,100,300,50);
        textField_nickname.setBounds(400,150,100,30);


        label_nickname.setFont(new Font(Font.MONOSPACED,Font.BOLD,20));
        label_nickname.setForeground(Color.CYAN);

        frame.getContentPane().add(jButton1);
        frame.getContentPane().add(label_nickname) ;
        frame.getContentPane().add(textField_nickname) ;

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(900, 500));
        frame.setResizable(false);
        frame.setVisible(true);

        jButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                if(!textField_nickname.getText().isEmpty()) {
                    //remove previous components
                    frame.remove(label_nickname);
                    frame.remove(jButton1);
                    frame.remove(textField_nickname);

                    Investor investor = new Investor(textField_nickname.getText());
                    StockExchangeDisplay la = new StockExchangeDisplay(investor, 900, 500);
                    la.init();
                    la.start();
                    frame.getContentPane().add(la, BorderLayout.CENTER);
                    new InvestorPanel(investor, la.getMarket(), frame);
                }
            }
        });
    }


}
