
/**
 *
 * StockExchangeDisplay class
 * - is responsible for every task related to chart generation including generation
 *
 * Wrote by @Phate
 */
package panel;

import currency.Bitcoin;
import data.*;
import investing.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

public class StockExchangeDisplay extends JApplet {
    private Market market;
    private Bitcoin bitcoin ;
    private Coordinate coordinate ;
    private ChartBox chartBox;
    private Investor investor;

    //window settings
    private int screenHeight;
    private int screenWidth;
    //chart positioning settings
    private int starting_y_position;
    private int startint_x_position;

    //chart details
    private int drawDelay;
    private int box_size ;
    private int space_between_boxes;
    private int max_box_to_draw;
    private int current_box_to_draw;

    public StockExchangeDisplay(Investor investor , int screenWidth , int screenHeight)
    {
        //screen settings
        this.screenWidth = screenWidth ;
        this.screenHeight = screenHeight;
        this.investor = investor;
    }

    @Override
    public void init(){
        //this.setRe
        this.setSize(screenWidth,screenHeight);

        //ChartBox settings
        this.chartBox = new ChartBox(screenWidth,screenHeight,400,450,4,10);

        //Market Settings
        this.bitcoin = new Bitcoin(1000);
        this.market = new Market(bitcoin ,investor);

        //chart positioning settings
        this.starting_y_position = chartBox.getBox_y_position() + chartBox.getBoxHeight()/2;
        this.startint_x_position = chartBox.getBox_x_position() + (chartBox.getBoxWidth()/10)*9;

        this.coordinate = new Coordinate(startint_x_position,starting_y_position);

        //chart details settings
        this.drawDelay = 1000;
        this.box_size = 3;
        this.space_between_boxes = 3;

        this.current_box_to_draw = 0 ;
        this.max_box_to_draw = (startint_x_position - chartBox.getBox_x_position())/(box_size + Math.abs(box_size-space_between_boxes));
    }
    @Override
    public void start(){
        new Thread(){
            public void run(){
                int delay = drawDelay - (drawDelay/10);
                generateChart(delay);
            }
        }.start();

        //Thread is paiting chart on the screen
        new Thread(){
            public void run(){
                while(true) {
                    try {
                        repaint();
                        Thread.sleep(drawDelay);
                    }
                    catch (Exception e){     System.out.println("Repainting Exception")      ;}
                }
            }
        }.start();
    }

    @Override
    public void paint(Graphics graphics){
        drawChartBox(graphics);
        drawChart(graphics);
        drawSubtitleInfo(graphics);
    }

    @Override
    public void update(Graphics graphics){
        drawChartBox(graphics);
        drawChart(graphics);
        drawSubtitleInfo(graphics);
    }

    private void drawChartBox(Graphics graphics){
        //graphic setting
        Graphics2D g2 = (Graphics2D) graphics;
        g2.setStroke(new BasicStroke(2));

        //clear ChartBox
        graphics.clearRect(chartBox.getBox_x_position()+1,chartBox.getBox_y_position(),chartBox.getBoxWidth() , chartBox.getBoxHeight());

        //draw middle Line
        graphics.drawLine(chartBox.getBox_x_position(),chartBox.getBox_y_position()+chartBox.getBoxHeight()/2,chartBox.getBox_x_position()+chartBox.getBoxWidth(),chartBox.getBox_y_position()+chartBox.getBoxHeight()/2);

        //draw lines between
        g2 = (Graphics2D) graphics;
        g2.setStroke(new BasicStroke(1));
        for(Integer line : chartBox.getLines()){
            graphics.drawLine(chartBox.getBox_x_position(),line,chartBox.getBox_x_position()+chartBox.getBoxWidth(),line);
        }

        //draw box
        g2 = (Graphics2D) graphics;
        g2.setStroke(new BasicStroke(chartBox.getFrameSize()));
        graphics.setColor(new Color(118, 118, 118));
        graphics.drawRect(chartBox.getBox_x_position(),chartBox.getBox_y_position(),chartBox.getBoxWidth(), chartBox.getBoxHeight());
    }

    private void drawChart(Graphics graphics){

        graphics.setColor(new Color(200, 29, 33));
        CopyOnWriteArrayList<data.Point> temp = coordinate.getPoints();
        int current_array_size = temp.size();
        int spaces = 0 ;

        int pts_to_draw = 0 ;

        if(current_box_to_draw >= max_box_to_draw && current_box_to_draw < current_array_size)
            pts_to_draw = max_box_to_draw;
        else
            pts_to_draw = current_array_size ;

        //drawing chart into screen
        for (int i = 0; i < pts_to_draw; i++) {
            if(current_box_to_draw - i < current_array_size && current_box_to_draw - i > 0  ) {
                graphics.fillRect(startint_x_position - spaces, temp.get(current_box_to_draw - i).getY(), box_size, box_size);
                spaces += space_between_boxes;
                //Setting current value exchange rate
                if(i == 0)
                    bitcoin.setCurrentValueBasingOnYPosition(starting_y_position - temp.get(current_box_to_draw).getY());
            }
        }
        //next
        if(current_box_to_draw < current_array_size)
            current_box_to_draw ++ ;
    }

    private void drawSubtitleInfo(Graphics graphics){
        //Adding Subtitles
        int space_between = 5 ;

        //setting font
        graphics.setFont(new Font(Font.MONOSPACED,Font.BOLD , 15));
        FontMetrics font_metrics = graphics.getFontMetrics();

        ArrayList<SubtitlePositioningHelper> subtitles = new ArrayList<>();
        subtitles.add(new SubtitlePositioningHelper("Investor: " + investor.getNickname(),space_between,font_metrics));
        subtitles.add(new SubtitlePositioningHelper("Avalible: $" , investor.getDollars(), space_between, font_metrics));
        subtitles.add(new SubtitlePositioningHelper("BTC: ", investor.getBitcoins(), space_between, font_metrics));
        subtitles.add(new SubtitlePositioningHelper("", space_between, font_metrics));
        subtitles.add(new SubtitlePositioningHelper("Statistics:", space_between, font_metrics));
        subtitles.add(new SubtitlePositioningHelper("Current rate: ", bitcoin.getCurrentExchangeRate(),space_between,font_metrics));
        subtitles.add(new SubtitlePositioningHelper("",  space_between , font_metrics));
        subtitles.add(new SubtitlePositioningHelper("Lowest rate in history: " , bitcoin.getLowestExchangeRate(), space_between ,font_metrics));
        subtitles.add(new SubtitlePositioningHelper("Highest rate in history: " , bitcoin.getHighestExchangeRate(), space_between, font_metrics));


        //Drawing subtitles
        for(SubtitlePositioningHelper subtitle : subtitles){
            graphics.clearRect(subtitle.getX_pos(), subtitle.getY_pos() - subtitle.getHeight(), 400, subtitle.getHeight());
            graphics.drawString(subtitle.getComunicate(), subtitle.getX_pos(), subtitle.getY_pos());
        }
        subtitles.get(0).resetAxis();

    }

    private void generateChart(int delay){
        Random random = new Random();
        coordinate.addPoint(0);

        int current_y_value = 0 ;
        int points_to_generate = Integer.MAX_VALUE - 1;
        for(int j = 0; j < points_to_generate; j++){
            int growing_tempo_chance = 10;
            int real_growing_temp0 = 7;
            if(succeed(50)){
                int scaleLenght = random.nextInt(growing_tempo_chance);
                for(int i = 0;i < scaleLenght ; i++){
                    if(succeed(50)){
                        if(safetyBoundDown(current_y_value)){
                            current_y_value -= real_growing_temp0;
                            coordinate.addPoint(current_y_value);
                        }
                        else {
                            current_y_value += real_growing_temp0;
                            coordinate.addPoint(current_y_value);
                        }
                    }
                    else{
                        if(safetyBoundUp(current_y_value)){
                            current_y_value += real_growing_temp0;
                            coordinate.addPoint(current_y_value);
                        }
                        else {
                            current_y_value -= real_growing_temp0;
                            coordinate.addPoint(current_y_value);
                        }
                    }
                }
            }
            else{
                int scaleLenght = random.nextInt(growing_tempo_chance);
                for(int i = 0; i < scaleLenght ; i++){
                    if(succeed(50)){
                        if(safetyBoundUp(current_y_value)){
                            current_y_value += real_growing_temp0;
                            coordinate.addPoint(current_y_value);
                        }
                        else{
                            current_y_value -= real_growing_temp0;
                            coordinate.addPoint(current_y_value);
                        }
                    }
                    else{
                        if(safetyBoundDown(current_y_value)){
                            current_y_value -= real_growing_temp0;
                            coordinate.addPoint(current_y_value);
                        }
                        else {
                            current_y_value += real_growing_temp0;
                            coordinate.addPoint(current_y_value);
                        }
                    }
                }
            }

            try{    Thread.sleep(delay)     ;}
            catch(Exception e){     System.out.println("can not delay chart generation");
            }
        }

    }

    private boolean safetyBoundUp(int current_y_pos){
        int temp = current_y_pos + chartBox.getBox_y_position()+chartBox.getBoxHeight()/2;

        if(temp <= (chartBox.getBox_y_position() + (chartBox.getBoxHeight()/20) )) {
            return true;
        }
        else
            return false;

    }

    private boolean safetyBoundDown(int current_y_pos){
        int temp = current_y_pos + chartBox.getBox_y_position()+chartBox.getBoxHeight()/2;

        if(temp >= (chartBox.getBoxHeight() - (chartBox.getBoxHeight()/20) )) {
            return true;
        }
        else
            return false;
    }

    private boolean succeed(int percent){
        Random random = new Random();
        if(percent > 100)
            percent = 100;
        else if(percent < 0)
            percent = 0 ;
        int range = random.nextInt(100);

        if (range < percent)
            return true;
        else
            return  false;
    }

    public Market getMarket(){
        return market;
    }
}
