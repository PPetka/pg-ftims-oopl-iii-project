package data;

import java.awt.*;


public class SubtitlePositioningHelper {
    private static int x_pos_helper = 10;
    private static int y_pos_helper = 50;

    private String comunicate;
    private int x_pos;
    private int y_pos;
    private int width;
    private int height;

    public SubtitlePositioningHelper(String comunicate, Integer spaceBetween, FontMetrics fontMetrics){
        this.height = fontMetrics.getHeight();

        //passing string witought number
        this.width = fontMetrics.stringWidth(comunicate+1);
        this.comunicate = comunicate ;

        x_pos = x_pos_helper;
        y_pos = y_pos_helper;

        y_pos_helper += (height +  spaceBetween);
    }

    public SubtitlePositioningHelper(String comunicate, Integer num_to_display, Integer spaceBetween, FontMetrics fontMetrics){
        this(comunicate,spaceBetween,fontMetrics);

        this.width = fontMetrics.stringWidth(comunicate + num_to_display.toString() + 1);
        this.comunicate = comunicate + num_to_display.toString();
    }

    public SubtitlePositioningHelper(String comunicate, Double num_to_display, Integer spaceBetween, FontMetrics fontMetrics){
        this(comunicate,spaceBetween,fontMetrics);

        this.width = fontMetrics.stringWidth(comunicate + num_to_display.toString() + 1);
        this.comunicate = comunicate + num_to_display.toString();
    }

    public void resetAxis(){
        x_pos_helper = 10;
        y_pos_helper = 50;
    }

    public int getX_pos() {
        return x_pos;
    }

    public int getY_pos() {
        return y_pos;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight(){
        return height;
    }

    public String getComunicate() {
        return comunicate;
    }
}
