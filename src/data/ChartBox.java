package data;

import java.util.ArrayList;

public class ChartBox {
    private int box_x_position;
    private int box_y_position;

    private int boxHeight;
    private int boxWidth ;

    private int numberOfLines ;
    private int frameSize ;

    private ArrayList<Integer> lines_y ;

    public ChartBox(int screenWidth, int screenHeight , int boxWidth, int boxHeight , int frameSize ,int numberOfLines){
        this.numberOfLines = numberOfLines;
        this.frameSize = frameSize ;
        this.box_x_position = screenWidth - (screenWidth/30) - boxWidth ;
        this.box_y_position = (screenHeight/30) ;
        this.boxHeight = boxHeight;
        this.boxWidth = boxWidth;
        this.lines_y = new ArrayList<>();
        calculateLines();
    }

    public void calculateLines(){
        int distance_between_lines = (boxHeight/2) / (numberOfLines+1);

        for(int i = 1; i <= numberOfLines ; i++){
            lines_y.add(box_y_position + (boxHeight/2) + distance_between_lines * i );
            lines_y.add(box_y_position + (boxHeight/2) - distance_between_lines * i );
        }

    }

    public ArrayList<Integer> getLines(){
        return lines_y;
    }

    public int getBox_x_position() {
        return box_x_position;
    }

    public int getBox_y_position() {
        return box_y_position;
    }

    public int getBoxHeight() {
        return boxHeight;
    }

    public int getBoxWidth() {
        return boxWidth;
    }

    public int getFrameSize(){
        return frameSize ;
    }

}
