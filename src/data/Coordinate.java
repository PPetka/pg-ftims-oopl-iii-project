
package data;

import java.util.concurrent.CopyOnWriteArrayList;


public class Coordinate{
    private CopyOnWriteArrayList<Point> points ;

    private int starting_y_position;
    private int starting_x_position;


    public Coordinate(int starting_x_position, int starting_y_position){
        this.starting_x_position = starting_x_position;
        this.starting_y_position = starting_y_position;

        points = new CopyOnWriteArrayList<>();
    }

    public void addPoint(int y) {
        points.add(new Point(starting_x_position , starting_y_position+y));

    }

    public CopyOnWriteArrayList<Point> getPoints(){
        return points;
    }
}
